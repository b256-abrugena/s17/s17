// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userInfo() {
		let namePrompt = prompt("What is your name?");
		let agePrompt = prompt("How old are you?");
		let locationPrompt = prompt("Where do you live?");
		
		console.log("Hello, " + namePrompt);
		console.log("You are " + agePrompt + " years old.");
		console.log("You live in " + locationPrompt);
	}

	userInfo();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function favoritebands() {
		let numberOne = "5 Seconds of Summer";
		let numberTwo = "Nelly";
		let numberThree = "Panic! At the Disco";
		let numberFour = "The Vamps";
		let numberFive = "MEDUZA";

		console.log("1. " + numberOne);
		console.log("2. " + numberTwo);
		console.log("3. " + numberThree);
		console.log("4. " + numberFour);
		console.log("5. " + numberFive);
	}

	favoritebands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function movieOne() {
		let movieOneName = "Parasite";
		console.log("1. " + movieOneName);

		function rotTomOne() {
			let rotRatingOne = "99%";
			console.log("Rotten Tomatoes Rating: " + rotRatingOne); 
		}

		rotTomOne();
	}

	movieOne();

	function movieTwo() {
		let movieTwoName = "Forgotten";
		console.log("2. " + movieTwoName);

		function rotTomTwo() {
			let rotRatingTwo = "82%";
			console.log("Rotten Tomatoes Rating: " + rotRatingTwo); 
		}

		rotTomTwo();
	}

	movieTwo();

	function movieThree() {
		let movieThreeName = "Train to Busan";
		console.log("3. " + movieThreeName);

		function rotTomThree() {
			let rotRatingThree = "94%";
			console.log("Rotten Tomatoes Rating: " + rotRatingThree); 
		}

		rotTomThree();
	}

	movieThree();

	function movieFour() {
		let movieFourName = "Pandora";
		console.log("4. " + movieFourName);

		function rotTomFour() {
			let rotRatingFour = "69%";
			console.log("Rotten Tomatoes Rating: " + rotRatingFour); 
		}

		rotTomFour();
	}

	movieFour();

	function movieFive() {
		let movieFiveName = "#Alive";
		console.log("5. " + movieFiveName);

		function rotTomFive() {
			let rotRatingFive = "88%";
			console.log("Rotten Tomatoes Rating: " + rotRatingFive); 
		}

		rotTomFive();
	}

	movieFive();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");

	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();